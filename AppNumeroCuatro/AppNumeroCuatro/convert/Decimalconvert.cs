﻿using System;
using Xamarin.Forms;
using System.Globalization;

namespace AppNumeroCuatro.convert
{
    class Decimalconvert : IValueConverter
    {
      public object Converter ( object value, Type targetType,object parameter, CultureInfo culture)
        {
            var valor = (decimal)value;
            return valor.ToString("C");
        }
        public object ConverteRbACK(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string valor = (string)value;
            return valor.Remove("0");
        }
    }
}
